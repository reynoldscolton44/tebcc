# Code of Conduct for track.easterbunny.cc Repositories
We decided not to adopt a standard code of conduct that many other repositories use, as we think less is more. As such, we ask for 3 things from you when contributing to the track.easterbunny.cc Repositories.

## Don't be a jerk
Being a jerk is not fun for anyone, and ruins the open source contributing experience. Please don't be a jerk. You will be barred from making future contributions to the tracker if you're a jerk.

## Don't take criticism the wrong way
If we are giving you criticism, we are giving it for your code. Not for who you are. Please don't take criticism the wrong way - and know that we're doing it to ensure the tracker's code quality is held to a certain standard.

## Use common sense
Use some common sense when contributing to the tracker! If you think a comment might be offensive, don't post it. Don't share personal information about others. These examples are two out of many that can be considered common sense.

## Reporting violations
If you believe a certain member has been violating the code of conduct, please make an issue report about it. You can also email support {at} easterbunny {dot} cc to report a code of conduct violation, if you prefer email, or if there is information that cannot be disclosed publicly.
