# track.easterbunny.cc Codebase
This is the codebase for track.easterbunny.cc. It is updated about 2 days after every release of track.easterbunny.cc.

# Note about code maintainabilty
track.easterbunny.cc v4/v5 is based on a lot of spaghetti code and is borderline unmaintainable. v6 of TEBCC will be rewritten (likely using React) over summer 2021, so things will be much better.

# Current Repo information
This repository is hosting Version 5.4.0. An update to v5.5.1 on this repo will come after release.

# Important note!
Please read the Licensing section towards the bottom of this page for important information about how the tracker is licensed under the GNU AGPL v3.0.

# Related projects
The other tools and services used during production can be found here:

track.easterbunny.cc Twitter Bot can be viewed at https://gitlab.com/track-easterbunny-cc/twitter-bot

track.easterbunny.cc Route Compiler program can be viewed at https://gitlab.com/track-easterbunny-cc/route-compiler

track.easterbunny.cc Geo API can be viewed at https://gitlab.com/track-easterbunny-cc/geo-api

Note - All repos except for this one have NOT been updated to support v5 code changes.

The major difference is in the route compiler repo - the route format changed from v4 to v5, but currently the repo only has code to compile v4 routes. v4 routes are NOT compatible with v5.

The Twitter Bot is updated to accept v5 routes, but this code isn't online yet.

The Geo API has been updated with new data points (mainly the accuracy of the Geo IP), which is needed by v5. This code is not online yet.

As things stand, I'll be updating the repos sometime in early to mid April 2021, once the v5 run is done.

# About
track.easterbunny.cc is an ongoing project by owenthe.dev to provide an Easter Bunny tracking experience comparable to that of NORAD & Google. It's also an "indie tracker" (if you want to call it that), so it's mostly maintained and coded by 1 person (along with help from folks on the East's Tracking Discord).

After two runs, the codebase is now open source for all to view and extend upon.

# Warning!
While we're super proud that we can open source our tracker for all to view, if you're looking to make your own tracker and use the TEBCC codebase as the starting point, you should have enough technical qualifications to understand our source code and figure out how it works.

We will only respond to issues that relate to the actual code of the tracker. Please do not use issues for support inqueries such as "how do I set up the tracker?", or "how can I get rid of weather information?". Google is going to be your friend, and so is trial and error.

# Getting Started
To get started with track.easterbunny.cc, you'll need a few things:
* A computer
* Python 3
* Some sort of web server to host the tracker on (Apache, Nginx)
* A Google Maps API key (new accounts can get $300 of billing credit for 12 months, plus there's a $200 credit each month. You do need to input a credit/debit card, but if you're just tinkering with the code you won't have an issue.)
* A Dark Sky API key (new signups aren't happening as Dark Sky API is going offline at the end of 2021. You're welcome to modify the route compiler on your end for an API that fits your needs, or modify the tracker to not accept weather information for extended stop data.)
* A server setup to host the Geo API on (the Geo API we host is referrer restricted to track.easterbunny.cc. Make sure you set up your own copy of the Geo API!). You can host the tracker/Geo API on the same server (with virtual hosts), or host it on a separate server. You need to make sure the server supports WSGI extensions to run Python code.

Download the repo via Git (`git clone https://github.com/track-easterbunny-cc/tebcc.git`), the .zip file on this page, or in the releases tab. It's up to you. 

In the first few bits of the HTML code on index.html, input your Google Maps API key. In the section of JavaScript code where you configure environment variables, change the `baseurl` to whatever the base URL of the server is (`http://localhost:5000`, etc) on both index.html and countdown/index.html. Additionally, you'll want to change the base URL of the Geo API, plus the API key for the API.

(more info about environment variables can be found in the wiki page about this)

After that, the tracker should work!

The repo comes with the 2020 route, so if you set your clock back to April 11, 2020 at 2 AM EDT, the tracker should work without issue. If you want a newer route, or want to make your own route, please see the route compiler repo. There's tons of details in that repository about how you can go about making your own route.

## How the site works
track.easterbunny.cc is a client-side tracker, meaning all the computations are done by the client after an initial server load.

There are three phases of tracking. Countdown, pre-tracking, and tracking.

Countdown occurs on a separate page that doesn't have any of the tracking logic, but does have the same UI as the tracker. Once the countdown reaches zero and tracking has begun, the tracker then transitions into pre-tracking for 3 hours (at least, in our configuration), where the Easter Bunny makes final preparations for the tracker. Then, tracking begins.

During tracking, the tracker stays updated with a variety of timers and triggers that rely on the current UNIX time, and what "row" of the route the tracker is on.

The tracker is entirely based on unix timestamps (how many seconds it has been since 1/1/1970, 12 AM UTC). This also means changing the time on your computer to when tracking starts will start tracking. 

Otherwise, there's a bunch of timers and triggers that will hopefully start to make sense once you look at the code.

# Some other useful nuggets of information
In the repository, you'll find pages like faq.html, but then faq/index.html. The actual FAQ page (in this instance) is located at faq/index.html, we use faq.html to bounce people to that other page.

The tracker has error page files for HTTP Errors 400, 401, 403, 404, 500, and 503. When setting up your web server, we recommend also setting up these error pages as well. In Apache2, you can specify an error document by appending this into your site configuration: `ErrorDocument (error code) (path/to/error_code_page.html)`. For instance, for the 4040 page, you'll probably use this code: `ErrorDocument 404 /404.html`.

Each page of track.easterbunny.cc has headers that define caonical URLs, manifest URLs, icons, etc. We kept these hard linked to track.easterbunny.cc. If you want to modify & deploy the tracker on the web, you'll need to change these URLs.

track.easterbunny.cc uses a manifest file (/assets/manifest/manifest.json) that allows the tracker to be added to the homescreen as a web app. There is another hard linked URL in the manifest that you'll need to change if you deploy the tracker on the web.

The news page has most of the hard links to track.easterbunny.cc left alone, as we're going to assume that if you deploy this tracker online, you'll probably have your own news instead of ours. 

redirpage/index.html is a redirect page that we use to bounce people off of viewing the contents of /assets, and if people visit www.easterbunny.cc or easterbunny.cc. It's UNIX time based to do proper redirecting like the rest of the tracker.

We have included the route from the 2020 run of the tracker, mostly as an example to show how a compiled route should look. We strongly suggest that you do not try to modify the route.json file, and rather make and compile your own route.

We're aware that Dark Sky API signups are no longer a thing. We're looking into how we can futureproof the tracker in Version 5 to use something like Open Weather Map. Right now, you'll need to create your own weather implementation in the compiler.


# Licensing
track.easterbunny.cc is fully licensed under GNU AGPL v3.0. The GNU AGPL v3.0 is a strong copyleft license that follows the pay it forwards mantra.

The GNU AGPL v3.0 requires that if you modify our source code and then distribute that modified source code in an application, website, etc, then you need to publicly release your source code for others to see.

If you're tinkering around with the tracker for your own private use and don't put your modified tracker online, you don't have to distribute the source code. 

However, if you do tinker around with the tracker, and then want to make the tracker public, you must distribute the tracker source code under the license, regardless of how many lines of code you changed. Whether it's 1 or 1,000, you must distribute your source code. You must also indicate the changes that you made to the source code. Please read the full license text for more information.

If you are a large organization or website that wants to use & modify our source code for your product, you must distribute your source code. We do not offer exceptions of **ANY** kind to **ANY** company/corporation/person.

Distributing your modified source code means that you need to make your source code publicly. You can do this in a variety of ways: Uploading to GitLab/GitHub, having a .zip file on your server with the modified source code, etc. Make sure that your means of distributing your modified source complies with the GNU AGPL v3.0 license.

When distributing the tracker, you'll want to make sure that you modify the URLs that point to your source code repository. Those URLs can be found in index.html, countdown/index.html, license/index.html, faq/index.html, incompatiblebrowser/index.html, jsdisabled/index.html, 400.html, 401.html, 403.html, and 404.html.

We also have embedded the source code links in basically every file in the headers. These are all hard linked as well, including the license as well. Make sure you modify all of that as well.

# Contributing
Please see the CONTRIBUTING.md file for more information about contributing to the track.easterbunny.cc Repositories.

# Code of Conduct
Please see the CODE-OF-CONDUCT.md file for information about the code of conduct when contributing to the track.easterbunny.cc Repositories.

# Note about iconography
The basket icon that we use is free for use in non-commercial environments. Since we don't serve ads or make a profit, we are covered under non-commercial use. However, if you intend to modify & use this tracker in an environment where you'll be earning money (even 1 cent counts), you'll need to pay for the rights for the basket icon, or substitute your own 96x96 icon for usage (/assets/icons/basket-96.png).

# Future plans
After 2 successful runs, there's still a lot that can be improved.
* Adding some more QoL features
* Adding stuff around the site
* Making the design a whole lot better (I'm a developer, not a designer sadly)

# For more information...
The Wiki page has good information about environment variables that are used to configure parts of the tracker. We're planning to put more information in the Wiki about other topics, such as how the tracker works, etc. in the future.

