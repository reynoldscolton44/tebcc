# Contributing to track.easterbunny.cc main repo
Since track.easterbunny.cc is an open-source project, we also accept pull requests and issues to improve our code.

## Pull requests
Right now, we cannot fully accept pull-requests to the track.easterbunny.cc Codebase until mid to late May. We are starting TEBCC v5 development and figuring out the logistics of developing the tracker as openly as possible.

You can still make PRs, but they may need to wait until v5.0.0 (the first TEBCC build) is launched, and then go from there. We're still figuring out lots of little specifics.

Regardless, please follow the general process when contributing to the repo for merge requests:

* Fork the project (or download via Git, up to you depending on your experience & familiarity)
* Make sure you check out to the proper branch. We'll have more details about this shortly about how the branches will work with development.
* Make your changes in your fork
* Submit a merge request, and we will do a code review + testing on your code
* Merge request will be approved/denied depending on code review. We may also ask you other questions or point out some issues with the code before we merge the request.

If you have any questions, please submit an issue.

## Issues
Issues are to be used for issues with the code, or any other issues as described in the documentation. General support will not be offered in issues for the tracker.

Issue templates are available for Bugs, Feature Requests, and Setup Issues. Setup issues are only to be used if you've followed the instructions, tried troubleshooting, and are stuck at a dead-end that involves the code on our side. You can also start up a blank issue if the templates don't fit your needs.

Make sure you include as much detail as possible in issue reports.
